import pokemon.Pokemon;

public class Main {
    public static void main(String[] args) {
        Pokemon pokemon1  = new Pokemon();
        pokemon1.setName("Simomander");
        Pokemon pokemon2  = new Pokemon();
        pokemon2.setName("Oskarimander");

        while (true){
            try{
                pokemon1.attack(pokemon2);
                Thread.sleep(100);
                pokemon2.attack(pokemon1);
                Thread.sleep(100);
            }catch (Exception ignored) {}
        }
    }
}
