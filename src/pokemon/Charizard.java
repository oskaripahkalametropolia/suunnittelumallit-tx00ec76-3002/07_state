package pokemon;

public class Charizard implements PokemonState {
    private static PokemonState instance = null;
    private Pokemon context;
    private final String name = "Charizard";
    private PokemonState evolution = null;
    private RNG random = RNG.getInstance();
    private final int maxHealth = 534;
    private final int xpToEvolve = 24;
    private final int minDamage = 0;//39;
    private final int maxDamage = 84;

    public void setContext(Pokemon context) {
        this.context = context;
    }

    private Charizard(){}

    public static PokemonState getState() {
        if (instance == null) instance = new Charizard();
        return instance;
    }

    @Override
    public PokemonState getNextState() {
        return evolution;
    }

    @Override
    public int attack() {
        return random.nextInt(minDamage, maxDamage);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHealth() {
        return maxHealth;
    }

    @Override
    public int getXpToEvolve() {
        return xpToEvolve;
    }
}
