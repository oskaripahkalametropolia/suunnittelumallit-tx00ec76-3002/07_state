package pokemon;

public class Pokemon {
    private PokemonState state;
    private int xp = 0;
    private int hp;
    private String name = null;

    public Pokemon(){
        changeState(Charmander.getState());
        this.hp = state.getHealth();
    }

    public String getName(){
        return name != null ? String.format("%s (%s)",name, state.getName()) : state.getName();
    }

    public void setName(String name){
        this.name = name;
    }

    protected void changeState(PokemonState state){
        this.state = state;
        this.state.setContext(this);
    }

    private Boolean takeDamage(int amount){
        this.hp -= amount;
        return this.hp <= 0;
    }

    public void attack(Pokemon target){
        int damage = state.attack();
        Boolean targetDied = target.takeDamage(damage);
        System.out.printf("%s deals %d damage to %s.%n", this.getName(), damage, target.getName());
        if (targetDied){
            this.xp++;
            System.out.printf("\t%s died.%n", target.getName());
            this.checkEvolution();
            this.heal();
            target.revive();
        }
    }

    private void revive() {
        System.out.printf("\t%s was revived.%n",this.getName());
        hp = state.getHealth();
    }

    private void heal() {
        System.out.printf("\t%s healed to full health.%n",this.getName());
        hp = state.getHealth();
    }

    private void checkEvolution(){
        PokemonState nextState = state.getNextState();
        if (nextState == null) return;
        if (xp >= state.getXpToEvolve()){
            System.out.printf("\t%s Evolved to %s.%n", this.getName(), nextState.getName());
            this.changeState(nextState);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
