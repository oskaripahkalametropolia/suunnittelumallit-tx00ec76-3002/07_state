package pokemon;

import java.util.Random;

public class RNG {
    private static RNG instance = null;
    private Random random;

    private RNG(){
        random = new Random();
    }

    public static RNG getInstance(){
        if (instance == null){
            instance = new RNG();
        }
        return instance;
    }

    public int nextInt(int low, int high){
        return random.nextInt(high - low + 1) + low;
    }
}
