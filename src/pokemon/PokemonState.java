package pokemon;

public interface PokemonState {
    void setContext(Pokemon context);
    PokemonState getNextState();
    int attack();
    String getName();
    int getHealth();
    int getXpToEvolve();
}
