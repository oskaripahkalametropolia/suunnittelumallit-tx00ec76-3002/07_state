package pokemon;

public class Charmeleon implements PokemonState {
    private static PokemonState instance = null;
    private Pokemon context;
    private final String name = "Charmeleon";
    private PokemonState evolution = Charizard.getState();
    private RNG random = RNG.getInstance();
    private final int maxHealth = 405;
    private final int xpToEvolve = 14;
    private final int minDamage = 0;//28;
    private final int maxDamage = 64;

    public void setContext(Pokemon context) {
        this.context = context;
    }

    private Charmeleon(){}

    public static PokemonState getState() {
        if (instance == null) instance = new Charmeleon();
        return instance;
    }

    @Override
    public PokemonState getNextState() {
        return evolution;
    }

    @Override
    public int attack() {
        return random.nextInt(minDamage, maxDamage);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHealth() {
        return maxHealth;
    }

    @Override
    public int getXpToEvolve() {
        return xpToEvolve;
    }
}
