package pokemon;

public class Charmander implements PokemonState {
    private static PokemonState instance = null;
    private Pokemon context;
    private final String name = "Charmander";
    private PokemonState evolution = Charmeleon.getState();
    private RNG random = RNG.getInstance();
    private final int maxHealth = 309;
    private final int xpToEvolve = 6;
    private final int minDamage = 0;//21;
    private final int maxDamage = 52;

    public void setContext(Pokemon context) {
        this.context = context;
    }

    private Charmander(){}

    public static PokemonState getState() {
        if (instance == null) instance = new Charmander();
        return instance;
    }

    @Override
    public PokemonState getNextState() {
        return evolution;
    }

    @Override
    public int attack() {
        return random.nextInt(minDamage, maxDamage);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHealth() {
        return maxHealth;
    }

    @Override
    public int getXpToEvolve() {
        return xpToEvolve;
    }
}
